package ru.litegamemc.creative.kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import ru.litegamemc.creative.kits.utils.Kit;

public class CactusKit implements Kit
{
  @Override
  public void giveItems(final PlayerInventory inventory)
  {
    {
      final ItemStack item = new ItemStack(Material.STONE_SWORD);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setItem(0, item);
    }
    {
      final ItemStack item = new ItemStack(Material.CACTUS);
      final ItemMeta meta = item.getItemMeta();
      item.setItemMeta(meta);
      inventory.setHelmet(item);
    }
    {
      final ItemStack item = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addEnchant(Enchantment.THORNS, 2, true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
      item.setItemMeta(meta);
      inventory.setChestplate(item);
    }
    {
      final ItemStack item = new ItemStack(Material.CHAINMAIL_LEGGINGS);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addEnchant(Enchantment.THORNS, 2, true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
      item.setItemMeta(meta);
      inventory.setLeggings(item);
    }
    {
      final ItemStack item = new ItemStack(Material.CHAINMAIL_BOOTS);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addEnchant(Enchantment.THORNS, 2, true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
      item.setItemMeta(meta);
      inventory.setBoots(item);
    }
  }
  
  @Override
  public int getLevel()
  {
    return 15;
  }
  
  @Override
  public String getName()
  {
    return "Cactus";
  }
  
  @Override
  public Material getIcon()
  {
    return Material.CACTUS;
  }
}
